import 'dog.dart';

var lastID = 3;
var mockDogs = [Dog(id: 1, name: 'A', age: 8), Dog(id: 2, name: 'B', age: 5)];

int getNewID() {
  return lastID++;
}

Future<void> addNewDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    mockDogs.add(Dog(id: getNewID(), name: dog.name, age: dog.age));
  });
}

Future<void> saveDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDogs.indexWhere((item) => item.id == dog.id);
    mockDogs[index] = dog;
  });
}

Future<void> delDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDogs.indexWhere((item) => item.id == dog.id);
    mockDogs.removeAt(index);
  });
}

Future<List<Dog>> getDogs() {
  return Future.delayed(Duration(seconds: 1), () => mockDogs);
}
