bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 15);
}

bool everyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> fillerOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> findShortNamed(Iterable<User> users) {
  return users.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAges(Iterable<User> users) {
  return users.map((user) => '${user.name} is ${user.age}');
}

void main() {
  var users = [
    User(name: 'ABCDE', age: 14),
    User(name: 'BCDEF', age: 18),
    User(name: 'CDE', age: 18),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25)
  ];
  if (anyUserUnder18(users)) {
    print('Have any user under 18');
  }
  if (everyUserOver13(users)) {
    print('every users is over 13');
  }

  var AgeMoreThan21Users = fillerOutUnder21(users);
  for (var user in AgeMoreThan21Users) {
    print(user.toString());
  }

  var shortNamedUsers = findShortNamed(users);
  for (var user in shortNamedUsers) {
    print(user.toString());
  }

  var nameAndAges = getNameAndAges(users);
  for (var user in nameAndAges) {
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  @override
  String toString() {
    return '$name, $age';
  }
}
